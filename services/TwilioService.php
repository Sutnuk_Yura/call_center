<?php

namespace app\services;

use Twilio\Rest\Client;

class TwilioService {

    private $twilio;
    private $error;

    public function __construct()
    {
        $twilio_sid = 'AC16504670605436c0d2d7ef3d85ff4eb2';
        $twilio_token = 'f43c873f86a95ee945903faba86687fd';
        $this->twilio = new Client($twilio_sid, $twilio_token);
    }

    public function getCountries()
    {
        $availablePhoneNumbers = $this->twilio->availablePhoneNumbers
            ->read();

        $countries = [];
        foreach ($availablePhoneNumbers as $record) {
            $countries[$record->countryCode] = $record->countryCode;
        }

        return $countries;
    }

    public function getPhoneNumbersForCountry(string $countryCode)
    {
        try {
            $mobile = $this->twilio->availablePhoneNumbers($countryCode)
                ->mobile
                ->read();
        } catch (\Twilio\Exceptions\RestException $exception) {
            $this->error = $exception->getMessage();
            return false;
        }

        $numbers = [];

        foreach ($mobile as $record) {
            $numbers[$record->friendlyName] = $record->friendlyName;
        }

        return $numbers;
    }

    public function buyPhoneNumber(string $phoneNumber)
    {
        try {
            $incoming_phone_number = $this->twilio->incomingPhoneNumbers
                ->create(array("phoneNumber" => $phoneNumber)
                );
        } catch (\Twilio\Exceptions\RestException $exception) {
            $this->error = $exception->getMessage();
            return false;
        }

        return $incoming_phone_number->sid;
    }

    public function releasePhoneNumber(string $phoneNumberSID)
    {
        try {
            $this->twilio->incomingPhoneNumbers($phoneNumberSID)
                ->delete();
        } catch (\Twilio\Exceptions\RestException $exception) {
            $this->error = $exception->getMessage();
            return false;
        }

        return true;
    }

    public function getError()
    {
        return $this->error;
    }
}