<?php

namespace app\models;

use yii\db\ActiveRecord;

class PhoneNumbers extends ActiveRecord
{

    public function rules()
    {
        return [
            [['country', 'phone_number','phone_number_sid','agent_id'], 'required' ]
        ];
    }

}