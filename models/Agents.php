<?php

namespace app\models;

use yii\db\ActiveRecord;

class Agents extends ActiveRecord
{

    public function rules()
    {
        return [
            ['name', 'required'],
        ];
    }

    public function getPhoneNumbers()
    {
        return $this->hasMany(PhoneNumbers::className(), ['agent_id' => 'id']);
    }


}