<?php

namespace app\models\forms;

use yii\base\Model;


class AgentForm extends Model
{
    public $name;

    public function rules()
    {
        return [
            ['name', 'required'],
        ];
    }

    public function tableName()
    {
        return 'agents';
    }
}