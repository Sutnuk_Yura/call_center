<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%phone_numbers}}`.
 */
class m190913_105425_create_phone_numbers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%phone_numbers}}', [
            'id' => $this->primaryKey(),
            'country' => $this->string()->notNull(),
            'phone_number' => $this->string()->notNull(),
            'phone_number_sid' => $this->string()->notNull(),
            'agent_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-phone_numbers-agent_id',
            'phone_numbers',
            'agent_id'
            );

        $this->addForeignKey(
            'fk-phone_numbers-agent_id',
            'phone_numbers',
            'agent_id',
            'agents',
            'id'
            );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%phone_numbers}}');
    }
}
