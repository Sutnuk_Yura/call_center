<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%agents}}`.
 */
class m190911_145204_create_agents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%agents}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%agents}}');
    }
}
