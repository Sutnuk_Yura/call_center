<?php //$this->beginBlock('agents'); ?>

<div>
    <a href="<?= \yii\helpers\Url::to('/agent/create'); ?> " class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Add agent</a>
</div>
    <table class="table">
        <?php foreach($agents as $agent): ?>
            <tr>
                <td><?= \yii\helpers\Html::encode($agent->name)?> </td>

                <td>
                    <a href="<?= \yii\helpers\Url::to(['/agent/delete', 'id' => $agent->id]); ?>"> delete </a> |
                    <a href="<?= \yii\helpers\Url::to(['/agent/edit', 'id' => $agent->id]) ?>" > update </a> |
                    <a href="<?= \yii\helpers\Url::to(['/agent/show', 'id' => $agent->id]) ?>" > show </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>

<?php //$this->endBlock(); ?>
<!---->
<!--    ...-->
<!---->



