<h2> <?= \yii\helpers\Html::encode($agent->name) ?> - phone numbers </h2>
<div>
    <a href="<?= \yii\helpers\Url::to(['/phone/create', 'agent_id' => $agent->id]); ?> " class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Add phone number</a>
</div>
<table class="table">
    <tr>
        <td> Phone number</td>
        <td> Action </td>
    </tr>
    <?php foreach($phoneNumbers as $phoneNumber): ?>
        <tr>
            <td>
                <?= \yii\helpers\Html::encode($phoneNumber->phone_number) ?>
            </td>
            <td>
                <a href="<?= \yii\helpers\Url::to(['phone/release', 'phone_id' => $phoneNumber->id]) ?>"> Release </a>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
