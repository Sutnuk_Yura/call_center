<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['action' => ['/agent/update', 'id' => $agent->id], 'options' => ['method' => 'post']]) ?>
    <?= $form->field($agent, 'name') ?>
    <?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>
<?php ActiveForm::end() ?>