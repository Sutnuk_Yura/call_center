<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['action' => ['/phone/store'], 'options' => ['method' => 'post']]) ?>
    <?= $form->field($phone, 'country')->hiddenInput(['value'=> $country])->label(false) ?>
    <?= $form->field($phone, 'agent_id')->hiddenInput(['value' => $agent->id])->label(false) ?>
    <?= $form->field($phone, 'phone_number')->dropDownList($phoneNumbers) ?>
    <?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>
<?php ActiveForm::end() ?>