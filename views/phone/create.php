<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['action' => ['/phone/select-phone', 'id' => $agent->id], 'options' => ['method' => 'post']]) ?>
    <?= $form->field($phone, 'country')->dropDownList($countries) ?>
    <?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>
<?php ActiveForm::end() ?>