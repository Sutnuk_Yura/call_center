<?php

namespace app\controllers;

use app\models\Agents;
use app\models\forms\AgentForm;
use app\models\PhoneNumbers;
use app\services\TwilioService;
use yii\base\DynamicModel;
use yii\base\Module;
use yii\helpers\Url;
use yii\web\Controller;


class PhoneController extends Controller
{
    private $twilio;

    public function __construct($id, Module $module, TwilioService $twilioService, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->twilio = $twilioService;
    }

    public function actionCreate($agent_id)
    {
        $agent = Agents::findOne($agent_id);

        if(!$agent) {
            return $this->redirect(\Yii::$app->request->referrer);
        }

        $availableCountries = $this->twilio->getCountries();

        return $this->render('create', [
            'countries' => $availableCountries,
            'agent' => $agent,
            'phone' => new PhoneNumbers(),
        ]);
    }

    public function actionSelectPhone($id)
    {
        $agent = Agents::findOne($id);
        $data = \Yii::$app->request->post();

        $phoneNumbers = $this->twilio->getPhoneNumbersForCountry($data['PhoneNumbers']['country']);
        if ($phoneNumbers !== false) {
            return $this->render('phone-select', [
                'country' => $data['PhoneNumbers']['country'],
                'agent' => $agent,
                'phone' => new PhoneNumbers(),
                'phoneNumbers' => $phoneNumbers
            ]);
        } else {
            return 'twilio error: '.$this->twilio->getError();
        }

    }

    public function actionStore()
    {
        $data = \Yii::$app->request->post();
        $phoneNumber = new PhoneNumbers();
        $phoneNumber->load($data);

        $phoneNumberSid = $this->twilio->buyPhoneNumber($phoneNumber->phone_number);
        if(!$phoneNumberSid){
            return 'twilio error: '.$this->twilio->getError();
        }

        $phoneNumber->phone_number_sid = $phoneNumberSid;

        $phoneNumber->save();

        return $this->redirect(['/agent/show', 'id' => $phoneNumber->agent_id]);
    }

    public function actionRelease($phone_id)
    {
        $phoneNumber = PhoneNumbers::findOne($phone_id);
        if(!$phoneNumber)
            return $this->redirect(\Yii::$app->request->referrer);

        $success = $this->twilio->releasePhoneNumber($phoneNumber->phone_number_sid);
        if(!$success)
            return $this->twilio->getError();

        $phoneNumber->delete();
        return $this->redirect(['/agent/show', 'id' => $phoneNumber->agent_id]);
    }
}