<?php

namespace app\controllers;

use app\models\Agents;
use app\models\forms\AgentForm;
use app\services\TwilioService;
use yii\helpers\Url;
use yii\web\Controller;

class AgentController extends Controller
{
    private $twilio;

    public function __construct($id,  $module, TwilioService $twilioService, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->twilio = $twilioService;
    }

    public function actionIndex()
    {
        $agents = Agents::find()->all();

        return $this->render('index', [
            'agents' => $agents
        ]);
    }

    public function actionCreate()
    {
        $agentForm = new AgentForm();
        return $this->render('create', [
            'agentForm' => $agentForm
        ]);
    }

    public function actionStore()
    {

        $data = \Yii::$app->request->post();

        $agent = new Agents();
        $agent->load($data, 'AgentForm');
        if($agent->save()){
            return $this->redirect('/agent/index');

        }
        return $this->redirect(\Yii::$app->request->referrer);

    }

    public function actionEdit($id)
    {
        $agent = Agents::find()
            ->where(['id' => $id])
            ->one();

        if(!$agent) {
            return $this->redirect(\Yii::$app->request->referrer);
        }

        $agentForm = new AgentForm();
        return $this->render('edit', [
            'agentForm' => $agentForm,
            'agent' => $agent
        ]);
    }

    public function actionUpdate($id)
    {
        $data = \Yii::$app->request->post();

        $agent = Agents::find()
            ->where(['id' => $id])
            ->one();

        if($agent){
            $agent->load($data);
            if($agent->save()) {
                return $this->redirect('/agent/index');
            }
        }

        return $this->redirect(\Yii::$app->request->referrer);
    }

    public function actionDelete($id)
    {
        $agent = Agents::find()
            ->where(['id' => $id])
            ->with('phoneNumbers')
            ->one();

        if(!$agent) {
            return $this->redirect(\Yii::$app->request->referrer);
        }

        foreach($agent->phoneNumbers as $phoneNumber) {
            $this->twilio->releasePhoneNumber($phoneNumber->phone_number_sid);
            $phoneNumber->delete();
        }

        $agent->delete();
        return $this->redirect('/agent/index');
    }

    public function actionShow($id)
    {
        $agent = Agents::findOne($id);

        if(!$agent)
            return $this->redirect(\Yii::$app->request->referrer);

        return $this->render('show', [
            'agent' => $agent,
            'phoneNumbers' => $agent->phoneNumbers,
        ]);

    }
}